<?php

use Illuminate\Database\Seeder;
use Marquine\Etl\Etl;


class MarquineETLSeeder extends Seeder
{
    //FIXME need to use default framework connection
    var $config = [
        'driver' => 'mysql',
        'host' => 'localhost',
        'port' => '3306',
        'database' => 'ww_locations_api',
        'username' => 'homestead',
        'password' => 'secret',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Etl::service('db')->addConnection($this->config); // connection name: 'default'

        loadLocations();
        loadFloors();
        loadCountries();

        DB::select(
            'call set_floor_location_id()'
        );
    }
}

function loadCountries()
{
    $jobCountries = new Etl;
    $optionsCountries = ['key' => ['code']];
    $jobCountries->extract('json', 'corrected_countries.json')
        ->load('insert_update', 'countries', $optionsCountries)
        ->run();
}

function loadLocations()
{
    $jobLocations = new Etl;
    $optionsLocations = ['columns' => ['id', 'name', 'address', 'opening_date', 'country']];
    $jobLocations->extract('json', 'locations.json')
        ->load('insert_update', 'locations', $optionsLocations)
        ->run();
}

function loadFloors()
{
    $options = ['columns' => ['id', 'number', 'description', 'desks']];
    $job3 = new Etl();
    $job3->extract('json', 'locations.json');
    $job3->run();

    foreach ($job3->toArray() as $val) {
        $job5 = new Etl();
        $job5->extract('collection', $val['floor'], $options);
        $job5->load('insert_update', 'floors');
        $job5->run();
    }
}
