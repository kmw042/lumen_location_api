
## About Lumen_locations_api

## Functional Requirements
 - Consume weather data from https://openweathermap.org/.
 - Provide an HTTP GET /wind/{zipCode} method that takes a zipcode as a required path parameter and returns a wind resource.
 - Validates input data.
 - Response format should be JSON.
 - Cache the resource for 15 minutes to avoid expensive calls to the OpenWeatherMap API.
 - Provide a CLI command that will bust the cache if needed.
 - Response fields should include:
 - Wind Speed
 - Wind Direction

## Deployment Requirements:
- A MySql database instance
- A mysql user named 'homestead' with password 'secret'
- Php7.1+

## Config
- Execute the ww_locations_api.sql file to create the database. 
- Execute the ww_locations_api2.sql file to create the stored procedure to update the floors table. 
- Grant appropriate privileges to user homestead.

## Startup
Run 'php -S localhost:8000 -t public' from the lumen_location_api directory

## Loading/Updating data
To load (or update) the database, call the endpoint /load.  This will load locations.json and corrected_countries.json (in the public/ folder).

## Postman Collection
Also in the public directory is a folder named docs containing:

- Minimal documentation @ localhost:8000/docs
- A postman collection (collection.json) of the endpoints.

## Security Vulnerabilities

NOT SECURED -- NOT FOR PRODUCTION USE

## License

This is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
