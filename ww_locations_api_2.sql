DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `set_floor_location_id`()
    READS SQL DATA
BEGIN
UPDATE floors set location_id = (select id div 10);
END$$
DELIMITER ;