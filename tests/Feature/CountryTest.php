<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CountryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    
    public function testCreate()
    {
        $this->post('/country', ['name' => 'Sal', 'code' => 'XX'])
        ->assertJsonFragment(["name"=>"Sal","code"=>"XX","id"=>0]);

        $this->delete('/country', ['code' => 'XX']);
    }

    public function testDestroy()
    {
        $this->delete('/country', ['code' => 'US'])
        ->assertSeeText('Country removed successfully');
    }

    public function testIndex()
    {
        $this->get('/countries')
        ->assertJsonFragment(["name"=>"AndorrA","code"=>"AD"]);
    }

    public function testshowByCode()
    {
        $this->get('/country', ['code' => 'AD'])
        ->assertJsonFragment(["code"=>"AD"]);
            }

    public function testshowByName()
    {
        $this->post('/country/name', ['name' => 'Albania'])
        ->assertJsonFragment(["name"=>"Albania"]);
     }

    public function testUpdate()
    {
        $this->put('/country', ['code' => 'SU'])
        ->assertJsonFragment(["name"=>"SALLY"]);
    }
}
