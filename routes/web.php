<?php
use Illuminate\Support\Facades\Artisan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// load data files - updates existing records
Route::get('load', function () {
    Artisan::call('db:seed');
    dd("Done");
});

//basic routes

Route::get('/countries', 'CountryController@index');
Route::post('/country', 'CountryController@create');
Route::get('/country/{code}', 'CountryController@showByCode');
Route::put('/country/{code}', 'CountryController@update');
Route::delete('/country/{code}', 'CountryController@destroy');
Route::post('/country/name/{name}', 'CountryController@showByName');
Route::get('/country/{code}/locations', 'CountryController@showLocations');
Route::get('/country/{code}/locations/order/{col}/{direction}', 'CountryController@showLocationsOrdered');
Route::get('/country/{code}/locationCount', 'CountryController@getTotalLocations');
Route::get('/country/{code}/locations/future', 'CountryController@getCountryFutureLocations');
Route::post('/country/{code}/location', 'LocationController@create');

Route::get('/locations', 'LocationController@index');
Route::get('/location/{id}', 'LocationController@show');
Route::put('/location/{id}', 'LocationController@update');
Route::delete('/location/{id}', 'LocationController@destroy');
Route::get('/location/{id}/floors', 'LocationController@showFloors');
Route::get('/location/{id}/floorCount', 'LocationController@getTotalFloors');
Route::get('/location/{id}/deskCount', 'LocationController@getTotalDesks');
Route::get('/locations/future', 'LocationController@getFutureLocations');
Route::get('/locations/invalid', 'LocationController@getInvalidLocations');
Route::post('location/{id}/floor', 'FloorController@create');
Route::get('/location/{id}/floor/{number}', 'FloorController@show');
Route::put('/location/{id}/floor/{number}', 'FloorController@update');
Route::delete('/location/{id}/floor/{number}', 'FloorController@destroy');

Route::get('/floors', 'FloorController@showAll');


