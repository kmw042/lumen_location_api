<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// $app->group(['prefix'=>'v1'], function() use($app){
//     $app->get('/locations', 'LocationController@index');
//     $app->post('/location', 'LocationController@create');
//     $app->get('/location/{id}', 'LocationController@show');
//     $app->put('/location/{id}', 'LocationController@update');
//     $app->delete('/location/{id}', 'LocationController@destroy');
//     });
