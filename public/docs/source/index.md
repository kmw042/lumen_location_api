---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost:8000/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_c1c192be6869849e72ede138e91bd809 -->
## countries
> Example request:

```bash
curl -X GET -G "http://localhost:8000/countries" 
```

```javascript
const url = new URL("http://localhost:8000/countries");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "current_page": 1,
    "data": [
        {
            "id": 0,
            "name": "AndorrA",
            "code": "AD",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "United Arab Emirates",
            "code": "AE",
            "created_at": "2019-04-29 22:52:01",
            "updated_at": "2019-04-29 22:52:01"
        },
        {
            "id": 0,
            "name": "Afghanistan",
            "code": "AF",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "Antigua and Barbuda",
            "code": "AG",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "Anguilla",
            "code": "AI",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "Albania",
            "code": "AL",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "Armenia",
            "code": "AM",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "Netherlands Antilles",
            "code": "AN",
            "created_at": "2019-04-29 22:52:01",
            "updated_at": "2019-04-29 22:52:01"
        },
        {
            "id": 0,
            "name": "Angola",
            "code": "AO",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "Antarctica",
            "code": "AQ",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "Argentina",
            "code": "AR",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "American Samoa",
            "code": "AS",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "Austria",
            "code": "AT",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "Australia",
            "code": "AU",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 0,
            "name": "Aruba",
            "code": "AW",
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        }
    ],
    "first_page_url": "http:\/\/localhost\/countries?page=1",
    "from": 1,
    "last_page": 17,
    "last_page_url": "http:\/\/localhost\/countries?page=17",
    "next_page_url": "http:\/\/localhost\/countries?page=2",
    "path": "http:\/\/localhost\/countries",
    "per_page": 15,
    "prev_page_url": null,
    "to": 15,
    "total": 245
}
```

### HTTP Request
`GET countries`


<!-- END_c1c192be6869849e72ede138e91bd809 -->

<!-- START_7866498804561f39b88f676ac73d31f6 -->
## country
> Example request:

```bash
curl -X POST "http://localhost:8000/country" 
```

```javascript
const url = new URL("http://localhost:8000/country");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST country`


<!-- END_7866498804561f39b88f676ac73d31f6 -->

<!-- START_e3ede140b30cfdd82e7ca166d424e4e0 -->
## country/{code}
> Example request:

```bash
curl -X GET -G "http://localhost:8000/country/1" 
```

```javascript
const url = new URL("http://localhost:8000/country/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
[]
```

### HTTP Request
`GET country/{code}`


<!-- END_e3ede140b30cfdd82e7ca166d424e4e0 -->

<!-- START_0047433c935b8688eddf188f5628e5aa -->
## country/{code}
> Example request:

```bash
curl -X PUT "http://localhost:8000/country/1" 
```

```javascript
const url = new URL("http://localhost:8000/country/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT country/{code}`


<!-- END_0047433c935b8688eddf188f5628e5aa -->

<!-- START_267c291dba7d5560f32c3171bdb5dc5d -->
## country/{code}
> Example request:

```bash
curl -X DELETE "http://localhost:8000/country/1" 
```

```javascript
const url = new URL("http://localhost:8000/country/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE country/{code}`


<!-- END_267c291dba7d5560f32c3171bdb5dc5d -->

<!-- START_35e2f4e840bb5a18bd79e0956da38d4f -->
## country/name/{name}
> Example request:

```bash
curl -X POST "http://localhost:8000/country/name/1" 
```

```javascript
const url = new URL("http://localhost:8000/country/name/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST country/name/{name}`


<!-- END_35e2f4e840bb5a18bd79e0956da38d4f -->

<!-- START_a3fdd5b2ed6bc3c36c19c86d7681e99f -->
## country/{code}/locations
> Example request:

```bash
curl -X GET -G "http://localhost:8000/country/1/locations" 
```

```javascript
const url = new URL("http://localhost:8000/country/1/locations");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
[]
```

### HTTP Request
`GET country/{code}/locations`


<!-- END_a3fdd5b2ed6bc3c36c19c86d7681e99f -->

<!-- START_620fbe17812b8353755febb939657b63 -->
## country/{code}/locations/order/{col}/{direction}
> Example request:

```bash
curl -X GET -G "http://localhost:8000/country/1/locations/order/1/1" 
```

```javascript
const url = new URL("http://localhost:8000/country/1/locations/order/1/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
[]
```

### HTTP Request
`GET country/{code}/locations/order/{col}/{direction}`


<!-- END_620fbe17812b8353755febb939657b63 -->

<!-- START_86d2dc4b67fd5c755b13190b597b0d92 -->
## country/{code}/locationCount
> Example request:

```bash
curl -X GET -G "http://localhost:8000/country/1/locationCount" 
```

```javascript
const url = new URL("http://localhost:8000/country/1/locationCount");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "country": "1",
    "locations": 0
}
```

### HTTP Request
`GET country/{code}/locationCount`


<!-- END_86d2dc4b67fd5c755b13190b597b0d92 -->

<!-- START_ddac8f58e0cb02e12699729f4ffbe28c -->
## country/{code}/locations/future
> Example request:

```bash
curl -X GET -G "http://localhost:8000/country/1/locations/future" 
```

```javascript
const url = new URL("http://localhost:8000/country/1/locations/future");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
[]
```

### HTTP Request
`GET country/{code}/locations/future`


<!-- END_ddac8f58e0cb02e12699729f4ffbe28c -->

<!-- START_9971ff6561d483f0b2c7ce66de64e06c -->
## country/{code}/location
> Example request:

```bash
curl -X POST "http://localhost:8000/country/1/location" 
```

```javascript
const url = new URL("http://localhost:8000/country/1/location");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST country/{code}/location`


<!-- END_9971ff6561d483f0b2c7ce66de64e06c -->

<!-- START_8825503bf91e45f9ecccbb0451c642f7 -->
## locations
> Example request:

```bash
curl -X GET -G "http://localhost:8000/locations" 
```

```javascript
const url = new URL("http://localhost:8000/locations");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "name": "Dr. Raymond Gerhold I",
            "address": "7316 Dooley Way Suite 417\nWymanport, ID 07478-5651",
            "opening_date": "2000-07-12",
            "country": "SS",
            "created_at": "2019-04-29 23:02:02",
            "updated_at": "2019-04-29 23:02:02"
        },
        {
            "id": 2,
            "name": "Dr. Barry Gusikowski DVM",
            "address": "434 Emilio Mission\nJohnsonborough, UT 37148",
            "opening_date": "1971-09-15",
            "country": "NR",
            "created_at": "2019-04-29 23:02:02",
            "updated_at": "2019-04-29 23:02:02"
        },
        {
            "id": 3,
            "name": "Shannon Grady",
            "address": "808 Cummerata Radial\nWest Hollieshire, CO 70578-9688",
            "opening_date": "1970-04-09",
            "country": "CU",
            "created_at": "2019-04-29 23:02:02",
            "updated_at": "2019-04-29 23:02:02"
        },
        {
            "id": 4,
            "name": "Deshawn Beahan I",
            "address": "65472 Block Flats Suite 094\nSimonisland, NC 19677-5111",
            "opening_date": "2018-08-16",
            "country": "EC",
            "created_at": "2019-04-29 23:02:02",
            "updated_at": "2019-04-29 23:02:02"
        },
        {
            "id": 5,
            "name": "Giovanni Kautzer",
            "address": "7794 Santino Tunnel\nBurleyside, NV 77154-3568",
            "opening_date": "1976-07-05",
            "country": "VE",
            "created_at": "2019-04-29 23:02:02",
            "updated_at": "2019-04-29 23:02:02"
        },
        {
            "id": 6,
            "name": "Zoey Predovic II",
            "address": "124 Schmitt Club\nRauchester, OR 01876",
            "opening_date": "1990-12-30",
            "country": "MQ",
            "created_at": "2019-04-29 23:02:02",
            "updated_at": "2019-04-29 23:02:02"
        },
        {
            "id": 7,
            "name": "Marcelino Terry",
            "address": "6116 Lynch Isle\nKarliview, GA 54881",
            "opening_date": "1974-04-12",
            "country": "NI",
            "created_at": "2019-04-29 23:02:02",
            "updated_at": "2019-04-29 23:02:02"
        },
        {
            "id": 8,
            "name": "Shanie Rogahn",
            "address": "4989 Mazie Stream Suite 220\nNorth Olenton, NH 41221",
            "opening_date": "1972-01-22",
            "country": "BD",
            "created_at": "2019-04-29 23:02:02",
            "updated_at": "2019-04-29 23:02:02"
        },
        {
            "id": 9,
            "name": "Bart Shanahan",
            "address": "4983 Autumn Village Suite 959\nJoanieville, VA 48153",
            "opening_date": "1975-01-23",
            "country": "AI",
            "created_at": "2019-04-29 23:02:03",
            "updated_at": "2019-04-29 23:02:03"
        },
        {
            "id": 10,
            "name": "Sabina Huels",
            "address": "9746 Kaylin Union Suite 646\nPort Lucius, VA 67753-9519",
            "opening_date": "1971-05-02",
            "country": "BO",
            "created_at": "2019-04-29 23:02:03",
            "updated_at": "2019-04-29 23:02:03"
        }
    ],
    "first_page_url": "http:\/\/localhost\/locations?page=1",
    "from": 1,
    "last_page": 10,
    "last_page_url": "http:\/\/localhost\/locations?page=10",
    "next_page_url": "http:\/\/localhost\/locations?page=2",
    "path": "http:\/\/localhost\/locations",
    "per_page": 10,
    "prev_page_url": null,
    "to": 10,
    "total": 99
}
```

### HTTP Request
`GET locations`


<!-- END_8825503bf91e45f9ecccbb0451c642f7 -->

<!-- START_5f2eaa8cd873a5f4bd5c5f6619a0c809 -->
## location/{id}
> Example request:

```bash
curl -X GET -G "http://localhost:8000/location/1" 
```

```javascript
const url = new URL("http://localhost:8000/location/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "id": 1,
    "name": "Dr. Raymond Gerhold I",
    "address": "7316 Dooley Way Suite 417\nWymanport, ID 07478-5651",
    "opening_date": "2000-07-12",
    "country": "SS",
    "created_at": "2019-04-29 23:02:02",
    "updated_at": "2019-04-29 23:02:02"
}
```

### HTTP Request
`GET location/{id}`


<!-- END_5f2eaa8cd873a5f4bd5c5f6619a0c809 -->

<!-- START_f809e514363ff6e8043804a9bfb806f5 -->
## location/{id}
> Example request:

```bash
curl -X PUT "http://localhost:8000/location/1" 
```

```javascript
const url = new URL("http://localhost:8000/location/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT location/{id}`


<!-- END_f809e514363ff6e8043804a9bfb806f5 -->

<!-- START_d685081be245df9e1be0e72cac393364 -->
## location/{id}
> Example request:

```bash
curl -X DELETE "http://localhost:8000/location/1" 
```

```javascript
const url = new URL("http://localhost:8000/location/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE location/{id}`


<!-- END_d685081be245df9e1be0e72cac393364 -->

<!-- START_2e10384ac6b5258f0ca3ac7932a6c0a5 -->
## location/{id}/floors
> Example request:

```bash
curl -X GET -G "http://localhost:8000/location/1/floors" 
```

```javascript
const url = new URL("http://localhost:8000/location/1/floors");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
[
    {
        "id": 11,
        "location_id": 1,
        "number": 1,
        "description": "Floor 1",
        "desks": 120,
        "created_at": "2019-04-29 22:52:00",
        "updated_at": "2019-04-29 22:52:00"
    },
    {
        "id": 12,
        "location_id": 1,
        "number": 2,
        "description": "Floor 2",
        "desks": 200,
        "created_at": "2019-04-29 22:52:00",
        "updated_at": "2019-04-29 22:52:00"
    },
    {
        "id": 13,
        "location_id": 1,
        "number": 3,
        "description": "Floor 3",
        "desks": 120,
        "created_at": "2019-04-29 22:52:00",
        "updated_at": "2019-04-29 22:52:00"
    },
    {
        "id": 14,
        "location_id": 1,
        "number": 4,
        "description": "Floor 4",
        "desks": 100,
        "created_at": "2019-04-29 22:52:00",
        "updated_at": "2019-04-29 22:52:00"
    }
]
```

### HTTP Request
`GET location/{id}/floors`


<!-- END_2e10384ac6b5258f0ca3ac7932a6c0a5 -->

<!-- START_0b014c819ec9a82b326c43b025c3a7bc -->
## location/{id}/floorCount
> Example request:

```bash
curl -X GET -G "http://localhost:8000/location/1/floorCount" 
```

```javascript
const url = new URL("http://localhost:8000/location/1/floorCount");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "location": "1",
    "floors": 4
}
```

### HTTP Request
`GET location/{id}/floorCount`


<!-- END_0b014c819ec9a82b326c43b025c3a7bc -->

<!-- START_b22891bcc1a3ecbf59c3b12c166457c2 -->
## location/{id}/deskCount
> Example request:

```bash
curl -X GET -G "http://localhost:8000/location/1/deskCount" 
```

```javascript
const url = new URL("http://localhost:8000/location/1/deskCount");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "location": "1",
    "desks": 540
}
```

### HTTP Request
`GET location/{id}/deskCount`


<!-- END_b22891bcc1a3ecbf59c3b12c166457c2 -->

<!-- START_f7f6df6c640e424ad78be940defb01f5 -->
## locations/future
> Example request:

```bash
curl -X GET -G "http://localhost:8000/locations/future" 
```

```javascript
const url = new URL("http://localhost:8000/locations/future");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
[]
```

### HTTP Request
`GET locations/future`


<!-- END_f7f6df6c640e424ad78be940defb01f5 -->

<!-- START_23d9b87467daa1d248206ef17bcb8eee -->
## locations/invalid
> Example request:

```bash
curl -X GET -G "http://localhost:8000/locations/invalid" 
```

```javascript
const url = new URL("http://localhost:8000/locations/invalid");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
[
    {
        "id": 1,
        "name": "Dr. Raymond Gerhold I",
        "address": "7316 Dooley Way Suite 417\nWymanport, ID 07478-5651",
        "opening_date": "2000-07-12",
        "country": "SS",
        "created_at": "2019-04-29 23:02:02",
        "updated_at": "2019-04-29 23:02:02"
    },
    {
        "id": 18,
        "name": "Evan Watsica",
        "address": "66321 Pamela Burg Apt. 770\nLake Samsonmouth, FL 43920",
        "opening_date": "2007-06-30",
        "country": "BQ",
        "created_at": "2019-04-29 23:02:03",
        "updated_at": "2019-04-29 23:02:03"
    },
    {
        "id": 33,
        "name": "Cody Brown",
        "address": "6083 Devante Rest\nPresleybury, NM 93952",
        "opening_date": "2016-07-17",
        "country": "MF",
        "created_at": "2019-04-29 23:02:03",
        "updated_at": "2019-04-29 23:02:03"
    },
    {
        "id": 34,
        "name": "Brennan Legros",
        "address": "16997 Quinton Station\nCristburgh, VA 12540",
        "opening_date": "2006-06-10",
        "country": "SS",
        "created_at": "2019-04-29 23:02:03",
        "updated_at": "2019-04-29 23:02:03"
    }
]
```

### HTTP Request
`GET locations/invalid`


<!-- END_23d9b87467daa1d248206ef17bcb8eee -->

<!-- START_7c9e004df934c47c6b70710bb5be9ac9 -->
## location/{id}/floor
> Example request:

```bash
curl -X POST "http://localhost:8000/location/1/floor" 
```

```javascript
const url = new URL("http://localhost:8000/location/1/floor");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST location/{id}/floor`


<!-- END_7c9e004df934c47c6b70710bb5be9ac9 -->

<!-- START_3125c3870c3cfbbb618c796b61c3c953 -->
## location/{id}/floor/{number}
> Example request:

```bash
curl -X GET -G "http://localhost:8000/location/1/floor/1" 
```

```javascript
const url = new URL("http://localhost:8000/location/1/floor/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
[
    {
        "id": 11,
        "location_id": 1,
        "number": 1,
        "description": "Floor 1",
        "desks": 120,
        "created_at": "2019-04-29 22:52:00",
        "updated_at": "2019-04-29 22:52:00"
    }
]
```

### HTTP Request
`GET location/{id}/floor/{number}`


<!-- END_3125c3870c3cfbbb618c796b61c3c953 -->

<!-- START_661287f9fefe43e485d91b4789b51be5 -->
## location/{id}/floor/{number}
> Example request:

```bash
curl -X PUT "http://localhost:8000/location/1/floor/1" 
```

```javascript
const url = new URL("http://localhost:8000/location/1/floor/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT location/{id}/floor/{number}`


<!-- END_661287f9fefe43e485d91b4789b51be5 -->

<!-- START_64a3f77358c0be35023abaf297254b31 -->
## location/{id}/floor/{number}
> Example request:

```bash
curl -X DELETE "http://localhost:8000/location/1/floor/1" 
```

```javascript
const url = new URL("http://localhost:8000/location/1/floor/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE location/{id}/floor/{number}`


<!-- END_64a3f77358c0be35023abaf297254b31 -->

<!-- START_743e923a02665913fb057bd89d66fbb1 -->
## floors
> Example request:

```bash
curl -X GET -G "http://localhost:8000/floors" 
```

```javascript
const url = new URL("http://localhost:8000/floors");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "current_page": 1,
    "data": [
        {
            "id": 11,
            "location_id": 1,
            "number": 1,
            "description": "Floor 1",
            "desks": 120,
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 12,
            "location_id": 1,
            "number": 2,
            "description": "Floor 2",
            "desks": 200,
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 13,
            "location_id": 1,
            "number": 3,
            "description": "Floor 3",
            "desks": 120,
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 14,
            "location_id": 1,
            "number": 4,
            "description": "Floor 4",
            "desks": 100,
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 21,
            "location_id": 2,
            "number": 1,
            "description": "Floor 1",
            "desks": 140,
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 22,
            "location_id": 2,
            "number": 2,
            "description": "Floor 2",
            "desks": 180,
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 23,
            "location_id": 2,
            "number": 3,
            "description": "Floor 3",
            "desks": 140,
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 24,
            "location_id": 2,
            "number": 4,
            "description": "Floor 4",
            "desks": 180,
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 25,
            "location_id": 2,
            "number": 5,
            "description": "Floor 5",
            "desks": 200,
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        },
        {
            "id": 26,
            "location_id": 2,
            "number": 6,
            "description": "Floor 6",
            "desks": 200,
            "created_at": "2019-04-29 22:52:00",
            "updated_at": "2019-04-29 22:52:00"
        }
    ],
    "first_page_url": "http:\/\/localhost\/floors?page=1",
    "from": 1,
    "last_page": 31,
    "last_page_url": "http:\/\/localhost\/floors?page=31",
    "next_page_url": "http:\/\/localhost\/floors?page=2",
    "path": "http:\/\/localhost\/floors",
    "per_page": 10,
    "prev_page_url": null,
    "to": 10,
    "total": 301
}
```

### HTTP Request
`GET floors`


<!-- END_743e923a02665913fb057bd89d66fbb1 -->


