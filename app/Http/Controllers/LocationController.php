<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Location;
use App\Floor;
use Illuminate\Http\Request;

class LocationController extends Controller
{
   var $Floors;

   public function index()
   {
      $Locations = Location::paginate(10); 

      return response()->json($Locations);
   }

   public function getTotalDesks($id)
   {
      $Locations = Location::find($id);
      $desks = ['location' => $id, 'desks' => $Locations->Floors->sum('desks')];

      return response()->json($desks);
   }

   public function getTotalFloors($id)
   {
      $Locations = Location::find($id);
      $rv = ['location' => $id, 'floors' => $Locations->Floors->count()];

      return response()->json($rv);
   }

   public function getFutureLocations()
   {
      $date = now()->toDateTimeString('Y-m-d');
      $Locations = Location::all()->where('opening_date', '>', $date);

      return response()->json($Locations->sortBy('opening_date')->values());
   }

   public function getInvalidLocations()
   {
      $Locations = DB::select('select * from locations where country not in (select code from countries)');
      return response()->json($Locations);
   }

   public function create(Request $request)
   {
      $Location = new Location;

      $Location->name = $request->name;
      $Location->address = $request->address;
      $Location->country = $request->country;
      $Location->opening_date = $request->opening_date;

      $Location->save();

      return response()->json($Location);
   }

   public function show($id)
   {
      $Location = Location::find($id);

      return response()->json($Location);
   }

   public function showFloors($id)
   {
      $Floors = Floor::all()->where('location_id', '=', $id);

      return response()->json($Floors);
   }

   public function update(Request $request, $id)
   {
      $Location = Location::find($id);

      $Location->name = $request->input('name');
      $Location->address = $request->input('address');
      $Location->country = $request->input('country');
      $Location->opening_date = $request->input('opening_date');

      $Location->save();

      return response()->json($Location);
   }

   public function destroy($id)
   {
      $Location = Location::find($id);
      $Location->delete();

      return response()->json('Location removed successfully');
   }
}
