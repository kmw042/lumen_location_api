<?php

namespace App\Http\Controllers;

use App\Country;
use App\Location;
use Illuminate\Http\Request;

class CountryController extends Controller
{
   var $Locations;

   public function index()
   {
      $Countrys = Country::paginate(15);

      return response()->json($Countrys);
   }

   public function create(Request $request)
   {
      $Country = new Country;
      $Country->name =  $request->name;
      $Country->code = $request->code;

      $Country->save();

      return response()->json($Country);
   }

   public function showByName($name)
   {
      $Countries = Country::all()->where('name', '=', (String)$name);

      return response()->json($Countries);
   }

   public function showByCode($code)
   {
      $Country = Country::all()->where('code', '=', (String)$code);

      return response()->json($Country);
   }

   public function getCountryFutureLocations($code)
   {
      $date = now()->toDateTimeString('Y-m-d');
      $Locations = Location::all()->where('country', '=', $code)->where('opening_date', '>', $date);

      return response()->json($Locations);
   }

   public function getTotalLocations($code)
   {
      $Locations = Location::all()->where('country', '=', (String)$code);
      $rv = ['country' => $code, 'locations' => $Locations->count()];

      return response()->json($rv);
   }
   public function showLocationsOrdered($code, $col, $direction)
   {
      $Locations = Location::all()->where('country', '=', (String)$code);
      $rv = null;

      if ($direction == 'asc') {
         $rv =  response()->json($Locations->sortBy($col)->values());
      } else {
         $rv =  response()->json($Locations->sortByDesc($col)->values());
      }
      return $rv;
   }

   public function showLocations($code)
   {
      $Locations = Location::all()->where('cou ntry ', '=', (String)$code); //paginate(10);

      return response()->json($Locations);
   }

   public function update(Request $request, $code)
   {
      $Countries = Country::all()->where(' code ', '=', (String)$code);
      $Country = $Countries->first;
      $Country->name = $request->name;
      $Country->code = $request->code;

      $Country->save();

      return response()->json($Country);
   }

   public function destroy($code)
   {
      $Country = Country::where(' code ', '=', (String)$code);
      $Country->delete();

      return response()->json('Country ' + $code + ' removed successfully');
   }
}
