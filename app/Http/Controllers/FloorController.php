<?php

namespace App\Http\Controllers;

use App\Floor;
use Illuminate\Http\Request;

class FloorController extends Controller
{

   public function index()
   {
      $Floors = Floor::all();

      return response()->json($Floors);
   }

   public function create(Request $request)
   {
      $Floor = new Floor;

      $Floor->location_id =  $request->location_id;
      $Floor->id = $request->id . $request->number;
      $Floor->number = $request->number;
      $Floor->description = $request->description;
      $Floor->desks = $request->desks;

      $Floor->save();

      return response()->json($Floor);
   }

   public function show($id, $number)
   {
      $Floor = Floor::all()->where('location_id', '=', $id)->where('number', '=', $number);

      return response()->json($Floor);
   }

   public function showAll()
   {
      $Floor = Floor::paginate(10);

      return response()->json($Floor);
   }

   public function update(Request $request, $loc_id, $number)
   {
      $id = $loc_id . $number;
      $Floor = Floor::find($id); //s->first(); 

      $Floor->description = $request->input('description');
      $Floor->desks = $request->input('desks');
      $Floor->save();

      return response()->json($Floor);
   }

   public function destroy($loc_id, $number)
   {
      $id = $loc_id . $number;
      $Floor = Floor::find($id);

      $Floor->delete();

      return response()->json('Floor removed successfully');
   }
}
